#!/bin/bash
#
# Set up app.

set -e -x

environment=$1

echo '#!/bin/bash
# Load bashrc if needed
if [ -f ~/.profile ]; then
   source ~/.profile
fi' >> ~/.bash_profile

echo 'export FLASK_APP="snakeoil.py"' >> ~/.bash_profile

echo '
# Prompt
export ENV_LABEL="SnakeOil App Server ('$environment') `getconf LONG_BIT`-bit"
GIT_PS1_SHOWDIRTY_STATE=true
if [ "$ENV_LABEL" ]; then
  PS1="\n\[\e[0;37m\][$ENV_LABEL] \n"
else
  PS1="\n\[\e[;33m\]\h\[\e[m\]::"
fi

# Git prompt
function __git_prompt {
  GIT_PS1_SHOWDIRTYSTATE=1
  [ `git config user.pair` ] && GIT_PS1_PAIR="`git config user.pair`@"
  __git_ps1 " $GIT_PS1_PAIR%s" | sed "s/ \([+*]\{1,\}\)$/\1/"
}

# show username@server over SSH.
function __name_and_server {
  if [ -n "$SSH_CLIENT" ] || [ -n "$SSH_TTY" ]; then
    echo "`whoami`@`hostname -s` "
  fi
}

PS1="$BY\$(__name_and_server)$C\W$G\$(__git_prompt)$RESET$R →$RESET "

bash_prompt() {
  # Regular colors
  local K="\[\033[0;30m\]"    # black
  local R="\[\033[0;31m\]"    # red
  local G="\[\033[0;32m\]"    # green
  local Y="\[\033[0;33m\]"    # yellow
  local B="\[\033[0;34m\]"    # blue
  local M="\[\033[0;35m\]"    # magenta
  local C="\[\033[0;36m\]"    # cyan
  local W="\[\033[0;37m\]"    # white

  # Emphasized (bolded) colors
  local BK="\[\033[1;30m\]"
  local BR="\[\033[1;31m\]"
  local BG="\[\033[1;32m\]"
  local BY="\[\033[1;33m\]"
  local BB="\[\033[1;34m\]"
  local BM="\[\033[1;35m\]"
  local BC="\[\033[1;36m\]"
  local BW="\[\033[1;37m\]"

  # Reset
  local RESET="\[\033[0;37m\]"

  PS1="$BY\$(__name_and_server)$C\W$G\$(__git_prompt)$RESET$R →$RESET "
}

bash_prompt
unset bash_prompt
' >> ~/.bash_profile

source ~/.bash_profile
