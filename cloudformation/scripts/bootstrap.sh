#!/bin/bash
#
# Install main dependencies.

set -e -x

ENVIRONMENT=$1
SERVER_NAME=$2
BUCKET=$3

if [ -z "$ENVIRONMENT" ]; then ENVIRONMENT=production; fi
if [ -z "$SERVER_NAME" ]; then SERVER_NAME=snakeoil.stagingmachine.site; fi
if [ -z "$BUCKET" ]; then BUCKET=snakeoil-deployment; fi

export SCRIPTS_URL=s3://$BUCKET/scripts
export DEBIAN_FRONTEND=noninteractive
export DEBCONF_NONINTERACTIVE_SEEN=true
export LANG=en_US.UTF-8
export LANGUAGE=en_US:en
export LC_ALL=en_US.UTF-8

# Install basic dependencies
apt-get update -qq \
  && apt-get -y -o Dpkg::Options::="--force-confdef" \
     -o Dpkg::Options::="--force-confold" upgrade \
  && apt-get -y -o Dpkg::Options::="--force-confdef" \
     -o Dpkg::Options::="--force-confold" dist-upgrade \
  && apt-get install -y ntp locales git ruby python3 python3-pip supervisor \
       build-essential libssl-dev libffi-dev python-dev libpq-dev xmlsec1

echo 'en_US.UTF-8 UTF-8' >> /etc/locale.gen \
  && locale-gen && dpkg-reconfigure locales

# Set timezone
echo 'US/Mountain' > /etc/timezone \
  && dpkg-reconfigure tzdata

# Add 1G swap and make it mount automatically on reboot
# Pulled from https://www.digitalocean.com/community/tutorials/how-to-add-swap-space-on-ubuntu-18-04
if [ ! -f /swapfile ]; then
  fallocate -l 1G /swapfile
  chmod 600 /swapfile
  mkswap /swapfile
  swapon /swapfile
  cp /etc/fstab /etc/fstab.bak
  echo '/swapfile none swap sw 0 0' >> /etc/fstab
  sysctl vm.swappiness=10
  sysctl vm.vfs_cache_pressure=50
  echo 'vm.swappiness=10' >> /etc/sysctl.conf
  echo 'vm.vfs_cache_pressure=50' >> /etc/sysctl.conf
fi

## Nginx
wget https://nginx.org/keys/nginx_signing.key -O - | apt-key add -
echo "deb http://nginx.org/packages/ubuntu/ xenial nginx
deb-src http://nginx.org/packages/ubuntu/ xenial nginx" > /etc/apt/sources.list.d/nginx.list
nginx=stable # use nginx=development for latest development version
add-apt-repository -y ppa:nginx/$nginx
apt-get update -y
apt-get install nginx -y
if [ ! -d /etc/nginx/sites-available ]; then mkdir /etc/nginx/sites-available; fi
if [ ! -d /etc/nginx/sites-enabled ]; then mkdir /etc/nginx/sites-enabled; fi

mkdir -p /srv/snakeoil
chown -R ubuntu: /srv/snakeoil

aws s3 cp $SCRIPTS_URL/snakeoil.com.conf /etc/nginx/sites-available/ \
  && sed -i "s/{{SERVER_NAME}}/$SERVER_NAME/g" /etc/nginx/sites-available/snakeoil.com.conf \
  && cd /etc/nginx/sites-enabled \
  && rm /etc/nginx/sites-enabled/default \
  && rm -f snakeoil.com.conf \
  && ln -s ../sites-available/snakeoil.com.conf

aws s3 cp $SCRIPTS_URL/nginx.conf /etc/nginx/
service nginx stop
service nginx start

# Get supervisor config ready.
aws s3 cp $SCRIPTS_URL/snakeoil.supervisor.conf /etc/supervisor/conf.d/

cd /root

# Install CodeDeploy agent
region=$(curl -s http://169.254.169.254/latest/dynamic/instance-identity/document | grep region | awk -F\" '{print $4}')
wget "https://aws-codedeploy-${region}.s3.${region}.amazonaws.com/latest/install" \
  && chmod +x ./install \
  && ./install auto \
  && service codedeploy-agent start

# Set up prompt
cd /home/ubuntu

aws s3 cp $SCRIPTS_URL/instructions.sh /etc/profile.d/instructions.sh

if [ ! -f ~/prompt-setup.sh ]; then
  su - ubuntu -c "aws s3 cp $SCRIPTS_URL/prompt-setup.sh ./ \
    && chmod +x prompt-setup.sh \
    && ./prompt-setup.sh ${ENVIRONMENT}"
fi

# Environment variables
echo 'export FLASK_APP="snakeoil.py"' >> /etc/profile
