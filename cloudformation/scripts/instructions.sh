#!/bin/bash

echo
echo '-----------------------------------------------------------------'
echo ' SnakeOil Application Server'
echo
echo ' The app is located in /srv/snakeoil'
echo
echo ' Assuming you have copied/pulled the latest code to'
echo ' /srv/snakeoil, you can deploy the web application with the'
echo ' following commands:'
echo
echo ' sudo supervisorctl restart snakeoil'
echo
echo ' You can tail application logs by using journalctl:'
echo
echo '   journalctl -t snakeoil -f'
echo
echo ' These instructions are saved at /etc/profile.d/instructions.sh'
echo '-----------------------------------------------------------------'
echo
