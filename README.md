## Snake Oil

[![forthebadge](https://forthebadge.com/images/badges/approved-by-george-costanza.svg)](https://forthebadge.com)

A small Flask app for [The Wealthy Earth Store](https://thewealthyearth.store)

Just an attempt to get payments working with as little code as possible while still being production ready.
