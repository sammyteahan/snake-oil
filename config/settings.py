import os
from os.path import join, dirname

from dotenv import load_dotenv

env_path = join(dirname(__file__), "../.env")
load_dotenv(env_path)

# Flask
FLASK_DEBUG = os.environ.get("FLASK_DEBUG", 1)
SECRET_KEY = os.environ.get("SECRET_KEY", "sample-key")
LOG_LEVEL = "DEBUG"  # CRITICAL / ERROR / WARNING / INFO / DEBUG
SERVER_NAME = os.environ.get("SERVER_NAME", "http://localhost:5000")

# Flask-SQLAlchemy
SQLALCHEMY_TRACK_MODIFICATIONS = False
SQLALCHEMY_DATABASE_URI = os.environ.get("DATABASE_URI")

# Flask-Mail
MAIL_PORT = 587
MAIL_SERVER = "smtp.mailgun.org"
MAIL_DEFAULT_SENDER = "default@noreply.com"

# Celery
CELERY_BROKER_URL = os.environ.get("BROKER_URL")
CELERY_ACCEPT_CONTENT = ["json"]
CELERY_TASK_SERIALIZER = "json"
CELERY_RESULT_SERIALIZER = "json"
CELERY_REDIS_MAX_CONNECTIONS = 5

# Admins
ADMINS = {
    '0': {
        'name': 'Sam',
        'email': os.environ.get("ADMIN_1_EMAIL"),
        'subscribed': True,
    },
    '1': {
        'name': 'Jim',
        'email': os.environ.get("ADMIN_2_EMAIL"),
        'subscribed': True,
    }
}
