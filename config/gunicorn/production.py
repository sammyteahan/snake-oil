"""
gunicorn -c "python:config.gunicorn.production" "snakeoil.app:create_app()"
"""
bind = "127.0.0.1:8001"
accesslog = "-"
access_log_format = '%(h)s %(l)s %(u)s %(t)s "%(r)s" %(s)s %(b)s'

workers = 4
preload_app = True
