from flask import Flask, render_template

import stripe
from celery import Celery

from snakeoil.blueprints.user import user
from snakeoil.blueprints.admin import admin
from snakeoil.blueprints.product import product
from snakeoil.blueprints.purchase import purchase
from snakeoil.blueprints.user.models import User
from snakeoil.extensions import db, login_manager, mail, migrate


CELERY_TASK_LIST = {
    "snakeoil.blueprints.user.tasks",
    "snakeoil.blueprints.purchase.tasks",
}


def create_celery_app(app):
    """
    Create a new celery app and tie together the Celery config to the
    app's config. Wrap all tasks in the context of the application
    """
    celery = Celery(
        app.import_name,
        broker=app.config["CELERY_BROKER_URL"],
        include=CELERY_TASK_LIST,
    )
    celery.conf.update(app.config)

    class ContextTask(celery.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery.Task = ContextTask
    return celery


def create_app(settings_override=None):
    """
    Create a Flask application using the app factory pattern.

    :return: Flask app
    """
    app = Flask(__name__, instance_relative_config=True)

    app.config.from_object("config.settings")
    app.config.from_pyfile("settings.py", silent=True)

    if settings_override:
        app.config.update(settings_override)

    stripe.api_key = app.config.get("STRIPE_SECRET_KEY")

    error_templates(app)
    app.register_blueprint(user)
    app.register_blueprint(admin)
    app.register_blueprint(product)
    app.register_blueprint(purchase)
    extensions(app)
    authentication(app, User)

    return app


def extensions(app):
    """
    Register 0 or more extensions (mutates the app passed in).

    :param app: Flask application instance
    :return: None
    """
    db.init_app(app)
    login_manager.init_app(app)
    mail.init_app(app)
    migrate.init_app(app, db)

    return None


def authentication(app, user_model):
    """
    Initialize the Flask-Login extension (mutates the app passed in).

    :param app: Flask application instance
    :param user_model: Model that contains the authentication information
    :type user_model: SQLAlchemy model
    :return: None
    """
    login_manager.login_view = "user.login"

    @login_manager.user_loader
    def load_user(user_id):
        """
        flask_login is database agnostic, so it expects a little help in knowing
        how to retrieve a user. This allows us to use the `current_user` function.
        """
        return user_model.query.get(int(user_id))

    return None


def error_templates(app):
    """
    Register 0 or more custom error pages (mutates app)

    :param app: Flask application instance
    :return: None
    """

    def render_status(status):
        code = getattr(status, "code", 500)
        return render_template("errors/{0}.html".format(code)), code

    for error in [404, 500]:
        app.errorhandler(error)(render_status)

    return None
