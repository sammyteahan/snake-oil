from snakeoil.app import create_app, create_celery_app


##
# Entrypoint for celery worker
#
# celery -A snakeoil.celery_entry.celery worker -l INFO
#
app = create_app()
celery = create_celery_app(app)
