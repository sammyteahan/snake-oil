from flask import render_template

from flask_mail import Message

from snakeoil.extensions import mail
from snakeoil.celery_entry import celery
from snakeoil.blueprints.user.models import User


def send_email(subject, sender, recipient, text_body):
    msg = Message(subject, sender=sender, recipients=[recipient])
    msg.body = text_body
    mail.send(msg)


@celery.task(name="deliver_password_reset_email")
def deliver_password_reset_email(user_id, token):
    user = User.find_by_id(user_id)
    ctx = {"user": user, "token": token}

    send_email(
        "WealthyEarth Password Reset",
        "sam@mail.thewealthyearth.store",
        user.email,
        render_template("user/mail/password_reset.txt", ctx=ctx),
    )
