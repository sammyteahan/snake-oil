from flask import (
    Blueprint,
    request,
    render_template,
    flash,
    redirect,
    url_for,
)

from flask_login import login_user, logout_user

from lib.safe_next_url import safe_next_url
from snakeoil.blueprints.user.models import User
from snakeoil.blueprints.admin.decorators import anonymous_required
from snakeoil.blueprints.user.forms import (
    LoginForm,
    RegistrationForm,
    PasswordForgotForm,
    PasswordResetForm,
)

user = Blueprint("user", __name__, template_folder="templates")


@user.route("/login", methods=["GET", "POST"])
@anonymous_required()
def login():
    form = LoginForm(next=request.args.get("next"))
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user is None or not user.check_password(form.password.data):
            flash("Invalid email or password")
            return redirect(url_for("user.login"))
        login_user(user, remember=True)
        next_url = request.form.get("next")
        if next_url:
            return redirect(safe_next_url(next_url))
        return redirect(url_for("product.index"))
    return render_template("user/login.html", form=form)


@user.route("/register", methods=["GET", "POST"])
@anonymous_required()
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(email=form.email.data)
        user.set_password(form.password.data)
        user.save()
        flash("Success! Thanks for signing up!")
        return redirect(url_for("user.login"))
    return render_template("user/register.html", form=form)


@user.route("/forgot-password", methods=["GET", "POST"])
@anonymous_required()
def forgot_password():
    form = PasswordForgotForm()
    if form.validate_on_submit():
        User.initialized_password_reset(request.form.get("email"))
        flash("A password reset link has been sent to your email")
        return redirect(url_for("user.login"))
    return render_template("user/forgot.html", form=form)


@user.route("/reset-password", methods=["GET", "POST"])
@anonymous_required()
def reset_password():
    form = PasswordResetForm(reset_token=request.args.get("reset_token"))

    if form.validate_on_submit():
        user = User.deserialize_token(request.form.get("reset_token"))

        if user is None:
            flash("Your reset password reset link has expired")
            return redirect(url_for("user.forgot_password"))

        user.password_hash = User.encrypt_password(
            request.form.get("password"))
        user.save()

        if login_user(user):
            flash("Your password has been reset")
            return redirect(url_for("product.index"))

    return render_template("user/reset.html", form=form)


@user.route("/logout")
def logout():
    logout_user()
    return redirect(url_for("product.index"))
