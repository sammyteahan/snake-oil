from flask_wtf import FlaskForm
from wtforms.validators import (
    DataRequired,
    ValidationError,
    Email,
    EqualTo,
    Length
)
from wtforms import (
    StringField,
    PasswordField,
    SubmitField,
    HiddenField
)

from snakeoil.blueprints.user.models import User


class LoginForm(FlaskForm):
    next = HiddenField()
    email = StringField("Email", validators=[DataRequired(), Email()])
    password = PasswordField("Password", validators=[DataRequired()])
    submit = SubmitField("Sign In")


class RegistrationForm(FlaskForm):
    email = StringField("Email", validators=[DataRequired(), Email()])
    password = PasswordField("Password", validators=[DataRequired()])
    password_confirm = PasswordField(
        "Confirm Password", validators=[DataRequired(), EqualTo("password")]
    )
    submit = SubmitField("Register")

    # pylint: disable=R0201
    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError("That email is already in use")


class PasswordForgotForm(FlaskForm):
    email = StringField("Email", validators=[DataRequired(), Email()])
    submit = SubmitField("Submit")


class PasswordChangeForm(FlaskForm):
    password = PasswordField("Password", validators=[DataRequired()])
    password_confirm = PasswordField(
        "Confirm Password", validators=[DataRequired(), EqualTo("password")]
    )
    submit = SubmitField("Submit")


class PasswordResetForm(FlaskForm):
    reset_token = HiddenField()
    password = PasswordField("Password", [DataRequired(), Length(8, 128)])
    submit = SubmitField("Submit")
