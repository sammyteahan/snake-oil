from collections import OrderedDict

from flask import current_app
from flask_login import UserMixin
from itsdangerous import TimedJSONWebSignatureSerializer
from werkzeug.security import generate_password_hash, check_password_hash

from snakeoil.extensions import db


class User(db.Model, UserMixin):
    """
    The UserMixin from flask_login provides 4 attributes that are required
    in order for track the user through a session. These attributes are:
    `is_authenticated`, `is_active`, `is_anonymous`, and `get_id()`.
    """

    ROLE = OrderedDict(
        [
            ("member", "Member"),
            ("admin", "Admin"),
        ]
    )

    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), index=True, unique=True)
    password_hash = db.Column(db.String(255))
    role = db.Column(
        db.Enum(*ROLE, name="role_types", native_enum=False),
        index=True,
        nullable=False,
        server_default="member",
    )

    def __str__(self):
        return "<User {}>".format(self.email)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def save(self):
        """
        Save a model instance.

        :return: Model instance
        """
        db.session.add(self)
        db.session.commit()

        return self

    def serialize_token(self, expiration=3600):
        """
        Sign and create a token that can be used for things such as resetting
        a password or other tasks that involve a one-off token

        :param expiration: Seconds until token expires, default to 1 hour
        :type expiration: int
        :return: JSON
        """
        key = current_app.config.get("SECRET_KEY")
        serializer = TimedJSONWebSignatureSerializer(key, expiration)

        return serializer.dumps({"user_email": self.email}).decode("utf-8")

    @classmethod
    def deserialize_token(cls, token):
        """
        Obtain a user from de-serializing a signed token

        :param token: Signed token
        :type token: str
        :return: User instance or None
        """
        serializer = TimedJSONWebSignatureSerializer(
            current_app.config["SECRET_KEY"])

        try:
            decoded_payload = serializer.loads(token)

            return User.find_by_email(decoded_payload.get("user_email"))
        except Exception:
            return None

    @classmethod
    def find_by_email(cls, email):
        """
        Find a user by their email

        :param email: email
        :type email: str
        :return: User instance
        """
        return User.query.filter_by(email=email).first()

    @classmethod
    def find_by_id(cls, id):
        """
        Find a user by ID

        :param id: id
        :type id: int
        :return: User
        """
        return User.query.get(id)

    @classmethod
    def encrypt_password(cls, passwd):
        """
        Hash a plaintext string using PBKDF2.

        :param passwd: password in plaintext
        :type passwd: str
        :return: str
        """
        if passwd:
            return generate_password_hash(passwd)

        return None

    @classmethod
    def initialized_password_reset(cls, email):
        """
        Initiate password reset flow

        :param email:
        :type email: str
        :return: User
        """
        user = User.find_by_email(email)

        if not user:
            return

        reset_token = user.serialize_token()

        from snakeoil.blueprints.user.tasks import deliver_password_reset_email

        deliver_password_reset_email.delay(user.id, reset_token)

        return user
