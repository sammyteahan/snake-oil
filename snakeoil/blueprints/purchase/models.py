from datetime import datetime

from sqlalchemy.sql import func

from snakeoil.extensions import db
from snakeoil.blueprints.product.models import Product


class Purchase(db.Model):
    __tablename__ = "purchases"
    uuid = db.Column(db.String, primary_key=True)
    email = db.Column(db.String)
    product_id = db.Column(db.Integer, db.ForeignKey("products.id"))
    product = db.relationship(Product)
    downloads_remaining = db.Column(db.Integer, default=5)
    created_at = db.Column(
        db.DateTime,
        nullable=False,
        default=datetime.utcnow,
        server_default=func.now()
    )

    def __str__(self):
        return "<{0}-{1}>".format(self.uuid, self.email)

    def save(self):
        """
        Save a model instance.

        :return: Model instance
        """
        db.session.add(self)
        db.session.commit()

        return self

    def decrement_downloads(self):
        """
        Decrement remaining download count
        """
        self.downloads_remaining -= 1
        db.session.commit()

    @classmethod
    def find_by_uuid(self, uuid):
        """
        Find Purchase by UUID

        :param uuid:
        :type uuid: uuid
        :return: Purchase
        """
        return Purchase.query.get(uuid)
