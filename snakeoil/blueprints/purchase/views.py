from uuid import uuid4

from flask import (
    Blueprint,
    request,
    render_template,
    flash,
    jsonify,
    redirect,
    url_for,
    send_from_directory,
    make_response,
    current_app,
)

import stripe

from snakeoil.blueprints.product.models import Product
from snakeoil.blueprints.purchase.models import Purchase


purchase = Blueprint("purchase", __name__, template_folder="templates")


@purchase.route("/charge", methods=["POST"])
def charge():
    data = request.get_json()
    stripe_token = data["token"]
    email = data["email"]
    product = Product.query.get(data["productId"])

    try:
        stripe.Charge.create(
            amount=int(product.price * 100),
            card=stripe_token,
            currency="usd",
            description=email,
        )
    except stripe.error.CardError:
        flash("Your card could not be charged. Please try again.")
        return redirect(url_for("product.index"))

    purchase = Purchase(uuid=str(uuid4()), email=email, product=product)
    purchase.save()

    from snakeoil.blueprints.purchase.tasks import send_purchase_confirmation
    from snakeoil.blueprints.purchase.tasks import send_admin_notifications

    send_purchase_confirmation.delay(email, purchase.uuid)
    send_admin_notifications.delay(email, purchase.uuid)

    return make_response(jsonify(redirectURI="/thank-you"), 201)


@purchase.route("/purchases/<uuid>", methods=["GET"])
def download(uuid):
    try:
        purchase = Purchase.query.get(uuid)
        if purchase.downloads_remaining <= 0:
            return redirect(url_for("purchase.empty"))

        purchase.decrement_downloads()

        return send_from_directory(
            directory="static",
            filename=purchase.product.file_name,
            as_attachment=True
        )
    # pylint: disable=W0702
    except:
        msg = "DOWNLOAD ERROR - purchase: {}".format(purchase.uuid)
        current_app.logger.error(msg)
        flash("something went wrong, please contact support")
        return redirect(url_for("product.index"))


@purchase.route("/empty", methods=["GET"])
def empty():
    return render_template("purchase/empty.html")


@purchase.route("/thank-you", methods=["GET"])
def thanks():
    return render_template("purchase/thank-you.html")
