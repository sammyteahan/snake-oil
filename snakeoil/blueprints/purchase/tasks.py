from flask import render_template, current_app

from flask_mail import Message

from config import settings
from snakeoil.extensions import mail
from snakeoil.celery_entry import celery
from snakeoil.blueprints.purchase.models import Purchase


def send_email(subject, sender, recipient, text_body, html_body):
    msg = Message(subject, sender=sender, recipients=[recipient])
    msg.body = text_body
    msg.html = html_body
    mail.send(msg)


@celery.task(name="send_purchase_confirmation")
def send_purchase_confirmation(recipient, purchase_uuid):
    purchase = Purchase.find_by_uuid(purchase_uuid)
    send_email(
        "Your WealthyEarth Purchase",
        "sam@mail.thewealthyearth.store",
        recipient,
        render_template("purchase/mail/purchase_confirmation.txt", purchase=purchase),
        render_template("purchase/mail/purchase_confirmation.html", purchase=purchase),
    )


@celery.task(name="send_admin_notifications")
def send_admin_notifications(customer_email, purchase_uuid):
    ctx = {
        "customer_email": customer_email,
        "purchase": Purchase.find_by_uuid(purchase_uuid),
    }
    for _, admin in settings.ADMINS.items():
        if admin.get('subscribed'):
            send_email(
                "WealthyEarth Purchase Notification",
                "sam@mail.thewealthyearth.store",
                admin["email"],
                render_template("purchase/mail/purchase_admin_notification.txt", context=ctx),
                render_template("purchase/mail/purchase_admin_notification.html", context=ctx),
            )
