from flask import Blueprint, render_template
from flask_login import login_required

from snakeoil.blueprints.user.models import User
from snakeoil.blueprints.product.models import Product
from snakeoil.blueprints.purchase.models import Purchase
from snakeoil.blueprints.admin.decorators import role_required

admin = Blueprint("admin", __name__,
                  template_folder="templates", url_prefix="/admin")


@admin.before_request
@login_required
@role_required("admin")
def before_request():
    """ Protect all admin routes """
    pass


@admin.route("", methods=["GET"])
def index():
    """
    Display high level counts
    """
    context = {
        "products": Product.query.count(),
        "purchases": Purchase.query.count(),
        "users": User.query.count(),
    }
    return render_template("admin/index.html", context=context)


@admin.route("/sales")
def sales():
    context = {
        "purchases": Purchase.query.order_by(
            Purchase.created_at.desc()).limit(50),
    }
    return render_template("admin/sales.html", context=context)


@admin.route("/products")
def products():
    context = {
        "products": Product.query.all(),
    }
    return render_template("admin/products.html", context=context)


@admin.route("/users")
def users():
    context = {
        "users": User.query.all(),
    }
    return render_template("admin/users.html", context=context)
