from flask import Blueprint, render_template, current_app

from snakeoil.blueprints.product.models import Product

product = Blueprint("product", __name__, template_folder="templates")


@product.route("/", methods=["GET"])
@product.route("/products", methods=["GET"])
def index():
    context = {}
    context["products"] = Product.query.filter_by(is_active=True).all()
    return render_template("product/index.html", context=context)


@product.route("/products/<int:id>", methods=["GET"])
def show(id):
    context = {}
    product = Product.query.filter_by(id=id).first_or_404()
    context["product"] = product
    context["stripe_key"] = current_app.config.get("STRIPE_PUBLISHABLE_KEY")
    return render_template("product/show.html", context=context)
