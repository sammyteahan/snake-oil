from collections import OrderedDict

from snakeoil.extensions import db


class Product(db.Model):
    TYPE = OrderedDict(
        [
            ("pdf", "PDF"),
            ("video", "Video"),
        ]
    )

    __tablename__ = "products"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    file_name = db.Column(db.String)
    version = db.Column(db.String)
    is_active = db.Column(db.Boolean, default=True)
    price = db.Column(db.Float)
    thumbnail_url = db.Column(db.String(255))
    banner_url = db.Column(db.String(255))
    description_short = db.Column(db.String(255))
    description_long = db.Column(db.Text)
    product_type = db.Column(
        db.Enum(*TYPE, name="product_types", native_enum=False))

    def __repr__(self):
        return "<Product {}>".format(self.name)
