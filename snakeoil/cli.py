from snakeoil.app import create_app

##
# with the factory-pattern _and_ flask version 0.12, this
# needs to reference a file that instantiates the app
#
# this is primarily here so that `flask` commands work from
# project root while the virtual env is activated. the
# production version of the app will use gunicorn as the
# application server and won't use this for wsgi purposes
#
# example: `flask run`, `flask shell`, `flask db history`
#
app = create_app()
